-- we can read Lua syntax here!
return {
    ["font_embolden"] = 0,
    ["copt_line_spacing"] = 90,
    ["copt_page_margins"] = {
        [1] = 5,
        [2] = 10,
        [3] = 5,
        [4] = 10
    },
    ["render_mode"] = 0,
    ["highlight_drawer"] = "lighten",
    ["bookmarks"] = {},
    ["gamma"] = 1,
    ["font_size"] = 16,
    ["screen_mode"] = "landscape",
    ["inverse_reading_order"] = false,
    ["show_overlap_enable"] = false,
    ["last_xpointer"] = "/FictionBook/body/p[31]/text().0",
    ["stats"] = {
        ["performance_in_pages"] = {
            [1488730176] = 2,
            [1488730144] = 6,
            [1488730163] = 5
        },
        ["authors"] = "",
        ["total_time_in_sec"] = 71,
        ["notes"] = 0,
        ["title"] = "wallabako.log",
        ["highlights"] = 0,
        ["pages"] = 6,
        ["series"] = "",
        ["language"] = ""
    },
    ["gamma_index"] = 15,
    ["config_panel_index"] = 5,
    ["highlights_imported"] = true,
    ["line_space_percent"] = 90,
    ["header_font_face"] = "Noto Sans",
    ["floating_punctuation"] = 1,
    ["copt_screen_mode"] = "portrait",
    ["hyph_alg"] = "English_US_hyphen_(Alan).pdb",
    ["copt_font_weight"] = 0,
    ["copt_status_line"] = 0,
    ["font_face"] = "Noto Serif",
    ["copt_embedded_css"] = 0,
    ["percent_finished"] = 0.18658810325477,
    ["highlight"] = {},
    ["embedded_css"] = false,
    ["copt_font_size"] = 16,
    ["highlight_disabled"] = false,
    ["page_overlap_style"] = "dim",
    ["copt_view_mode"] = 1,
    ["css"] = "./data/txt.css",
    ["summary"] = {
        ["status"] = "complete",
        ["modified"] = ""
    },
    ["readermenu_tab_index"] = 3,
    ["copt_font_gamma"] = 15,
    ["rotation_mode"] = 1,
    ["bookmarks_sorted"] = true
}
